from imgurpython import ImgurClient
import os


def upload_picture(image_path):
    filepath = "imgurl_auth.txt"

    if not os.path.isfile(filepath):
        print("imgurl auth file path does not exist.")
        return

    with open(filepath, 'r') as f:
        client_id = f.readline().rstrip()
        client_secret = f.readline().rstrip()
        client = ImgurClient(client_id, client_secret)

    print("Uploading image... ")
    image = client.upload_from_path(image_path, anon=True)
    print("Done")
    return image['link']


if __name__ == "__main__":

    image_link = upload_picture('20190411_191536.jpg')

    print("Image was posted! Go check your images you sexy beast!")
    print("You can find it here: {0}".format(image_link))
