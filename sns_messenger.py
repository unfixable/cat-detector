from twilio.rest import Client
from upload_image_imgur import upload_picture
import os


def get_phone_numbers():
    phone_number_list = []
    filepath = "whatsapp_numbers.txt"

    if not os.path.isfile(filepath):
        print("config file path does not exist.")
        return 0, phone_number_list

    with open(filepath, 'r') as f:
        number_of_phones = int(f.readline())
        print("num = {}".format(number_of_phones))
        for i in range(number_of_phones):
            phone_number_list.append(f.readline().rstrip())
        print(phone_number_list)

        return number_of_phones, phone_number_list


def send_whatsapp_message(message_text, image_filename):
    # client credentials are read from TWILIO_ACCOUNT_SID and AUTH_TOKEN
    client = Client()

    number_of_phones, phone_numbers_list = get_phone_numbers()

    image_url = upload_picture(image_filename)

    for i in range(number_of_phones):
        from_whatsapp_number = 'whatsapp:+14155238886'
        to_whatsapp_number = 'whatsapp:' + phone_numbers_list[i]
        client.messages.create(body=message_text,
                               from_=from_whatsapp_number,
                               to=to_whatsapp_number,
                               media_url=[image_url],)


if __name__ == '__main__':
    send_whatsapp_message("Katze gefunden!", 'images/2019-04-14_18_54_31.jpg')
