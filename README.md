# Cat Detector

Personal project to detect cats using a raspberry pi 3 b+ and a generic ip camera.

The idea for this project came from my wife after her suspecting that cats living nearby are stealing from our cat's food~ turns out she was right :)


The raspberry pi runs a fast MobileNetSSD for detecting the cats at about 2-3 fps. 

If a cat is detected the camera image is saved and a notification is sent through whatsapp (using the twilio lib) to a list of numbers that can be configured.


**To-Do**: Train the network to recognize our own cat and then send some audio to shoo away other cats if they come to eat :D



**Screenshots:**

![](screenshots/screen1.jpg)
![](screenshots/screen2.png)
![](screenshots/screen3.jpg)
![](screenshots/screen4.jpg)
![](screenshots/screen5.jpg)