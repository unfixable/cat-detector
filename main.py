import cv2 as cv
import threading
import os
from datetime import datetime
from sns_messenger import send_whatsapp_message
import numpy as np
from object_detect import ObjectDetect
import time


def camera_thread(camera_name, camera_data):
    # Currently only STARCAM IP cameras are supported
    # If you want to add another cameras support you should find and add
    # specific RTSP video stream path for particular camera brend
    ip_address, port_number, username, password = str(camera_data[
        0]), str(camera_data[1]), str(camera_data[2]), str(camera_data[3])
    print("Camera thread starting....")
    print(ip_address)
    print(port_number)
    print("Trying to access smart camera")
    vcap = cv.VideoCapture("rtsp://" + username + ":" + password +
                           "@" + ip_address + ":" + port_number +
                           "/tcp/av0_1")

    COLORS = np.random.uniform(0, 255, size=(15, 3))
    od = ObjectDetect("cat", 0.5, False)
    t_previous_detection = time.time()
    framecounter = 0

    while (1):
        ret, frame = vcap.read()
        if frame is None:
            print("No camera frame received. Restarting camera.")
            start_cameras()
            break
        else:
            image_height, image_width, _ = frame.shape
            framecounter += 1

            if framecounter % 10 == 0:
                # t_start = time.time()
                found, bx, __ = od.detect(frame)
                # t_end = time.time()
                # print("Detection time is {}".format(t_end-t_start))
                if found:
                    for b in bx:
                        (startX, startY, endX, endY) = b.astype("int")
                        cv.rectangle(frame, (startX, startY),
                                     (endX, endY), COLORS[0], 2)

                        if(time.time() - t_previous_detection > 30):
                            t_previous_detection = time.time()
                            image_filename = 'images/' + \
                                str(datetime.now().date()) + '_'
                            image_filename += str(datetime.now().hour) + '_'
                            image_filename += str(datetime.now().minute) + '_'
                            image_filename += str(datetime.now().second) + '.jpg'
                            cv.imwrite(image_filename, frame)
                            message = "Ich habe eine Katze gefunden.\n"
                            message += "Uhrzeit: " + \
                                str(datetime.now().hour) + ":" + \
                                str(datetime.now().minute)
                            send_whatsapp_message(message, image_filename)

            # else:
                # print("Could not detect.")

            cv.imshow(camera_name + '_original', frame)
            cv.waitKey(1)


def get_cameras():
    camera_list = []
    filepath = "config.txt"

    if not os.path.isfile(filepath):
        print("config file path does not exist.")
        return 0, camera_list

    with open(filepath, 'r') as f:
        number_of_cameras = int(f.readline())
        print("num = {}".format(number_of_cameras))
        for i in range(number_of_cameras):
            camera_data = []
            ip = f.readline().rstrip()
            port = f.readline().rstrip()
            username = f.readline().rstrip()
            password = f.readline().rstrip()
            camera_data.append(ip)
            camera_data.append(port)
            camera_data.append(username)
            camera_data.append(password)
            camera_list.append(camera_data)
        print(camera_list)

        return number_of_cameras, camera_list


def start_cameras():
    cam_num, cam_list = get_cameras()

    for i in range(cam_num):
        threading.Thread(target=camera_thread, args=[
            "Starcam1", cam_list[i]]).start()


if __name__ == '__main__':
    start_cameras()
