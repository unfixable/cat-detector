import numpy as np
import imutils
import time
import cv2
import threading


class ObjectDetect:
    def __init__(self, target="person", confidence=0.5, debug=False):
        self.debug = debug
        self.confidence = confidence
        self.target = target
        self.CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
                        "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
                        "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
                        "sofa", "train", "tvmonitor"]
        self.COLORS = np.random.uniform(0, 255, size=(len(self.CLASSES), 3))
        # load our serialized model from disk
        t1 = time.time()
        print("[INFO] loading model....")
        self.net = cv2.dnn.readNetFromCaffe(
            "models/MobileNetSSD_deploy.prototxt.txt", "models/MobileNetSSD_deploy.caffemodel")
        print("Loading time is {}".format(time.time()-t1))

        # targets = [cv2.dnn.DNN_TARGET_CPU, cv2.dnn.DNN_TARGET_OPENCL, cv2.dnn.DNN_TARGET_OPENCL_FP16, cv2.dnn.DNN_TARGET_MYRIAD]
        # self.net.setPreferableTarget(targets[0])

    def __del__(self):
        print("Destructor:ObjectDetection")

    def detect(self, frame):
        object_found = False
        bx = []
        confidences = []

        (h_, w_) = frame.shape[:2]
        # print("width = {0} height = {1}".format(w, h))

        frame = imutils.resize(frame, width=400)
        # grab the frame dimensions and convert it to a blob
        (h, w) = frame.shape[:2]

        scale_val = h_ / h

        # print("width = {0} height = {1}".format(w, h))
        blob = cv2.dnn.blobFromImage(cv2.resize(
            frame, (300, 300)), 0.007843, (300, 300), 127.5)

        # pass the blob through the network and obtain the detections and
        # predictions
        self.net.setInput(blob)
        detections = self.net.forward()

        # loop over the detections
        for i in np.arange(0, detections.shape[2]):
            # extract the confidence (i.e., probability) associated with
            # the prediction
            confidence = detections[0, 0, i, 2]

            # filter out weak detections by ensuring the `confidence` is
            # greater than the minimum confidence
            if confidence > self.confidence:
                # extract the index of the class label from the
                # `detections`, then compute the (x, y)-coordinates of
                # the bounding box for the object
                idx = int(detections[0, 0, i, 1])

                if self.CLASSES[idx] != self.target:
                    continue

                object_found = True  # return value-1
                confidences.append(confidence)

                box = detections[0, 0, i, 3:7] * \
                    np.array([w, h, w, h])  # return value-2
                # 1.6 Scaling value comes from original image and resized image ratio
                bx.append(scale_val*box)

                if self.debug:
                    (startX, startY, endX, endY) = box.astype("int")
                    # draw the prediction on the frame
                    label = "{}: {:.2f}%".format(
                        self.CLASSES[idx], confidence * 100)
                    cv2.rectangle(frame, (startX, startY),
                                  (endX, endY), self.COLORS[idx], 2)
                    y = startY - 15 if startY - 15 > 15 else startY + 15
                    cv2.putText(frame, label, (startX, y),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.COLORS[idx], 2)
        if self.debug:
            # show the output frame
            cv2.imshow("Frame", frame)
            cv2.waitKey(0)

        return object_found, bx, confidences


def detect_image(image):
    COLORS = np.random.uniform(0, 255, size=(15, 3))

    od = ObjectDetect("person", 0.3, False)

    # frame = cv2.imread("images/pushup1.png", 1)
    cv2.imshow("original", image)
    # cv2.waitKey(1)
    t_start = time.time()
    found, bx, __ = od.detect(image)
    t_end = time.time()
    print("Detection time is {}".format(t_end-t_start))
    if found:
        for b in bx:
            (startX, startY, endX, endY) = b.astype("int")
            cv2.rectangle(image, (startX, startY), (endX, endY), COLORS[0], 2)
        cv2.imshow("Result", image)
        # cv2.waitKey(0)
    else:
        print("Could not detect.")
    cv2.waitKey(0)



if __name__ == '__main__':

    image = cv2.imread("images/2019-04-13_18_34_54.jpg", 1)
    detect_image(image)
    image = cv2.imread("images/2019-04-13_18_34_43.jpg", 1)
    detect_image(image)
    # detect_video()
    print("FIN")
